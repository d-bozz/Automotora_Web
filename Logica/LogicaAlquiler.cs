﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EntidadesCompartidas;
using Persistencia;

namespace Logica
{
    public class LogicaAlquiler
    {
        

        public static void RealizarAlquiler(Alquiler a)
        {
            try
            {
                PersistenciaAlquiler.RealizarAlquiler(a);
            }
            catch(Exception ex)
            { throw ex; }
        }

        public static List<Alquiler> AlquileresPorVehiculo(string pMatricula)
        {
            return PersistenciaAlquiler.AlquileresPorVehiculo(pMatricula);

        }
    }
}
