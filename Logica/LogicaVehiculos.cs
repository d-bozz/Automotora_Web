﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Persistencia;
using EntidadesCompartidas;

namespace Logica
{
    public class LogicaVehiculos
    {

        public static List<Vehiculo> ListarVehiculos()
        {
            try
            {
                List<Vehiculo> listaVehiculos = new List<Vehiculo>();
                listaVehiculos = PersistenciaUtilitarios.ListarUtilitarios();
                listaVehiculos.AddRange(PersistenciaAutos.ListarAutos());
                return listaVehiculos;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public static Vehiculo Buscar(string pMatricula)
        {
            Vehiculo v = PersistenciaAutos.Buscar(pMatricula);
            if (v == null)
                v=PersistenciaUtilitarios.Buscar(pMatricula);
            return v;
        }
        public static void Modificar(Vehiculo v)
        {
            if (v is Auto)
                PersistenciaAutos.Modificar((Auto)v);
            else if (v is Utilitario)
                PersistenciaUtilitarios.Modificar((Utilitario)v);
        }
        public static void Agregar(Vehiculo v)
        {
            if (v is Auto)
                PersistenciaAutos.Agregar((Auto)v);
            else if (v is Utilitario)
                PersistenciaUtilitarios.Agregar((Utilitario)v);
        }
        public static void Eliminar(Vehiculo v)
        {
            if (v is Auto)
                PersistenciaAutos.Eliminar(v.Matricula);
            else if (v is Utilitario)
                PersistenciaUtilitarios.Eliminar(v.Matricula);
        }

        public static List<Vehiculo> ListarDisponibles(DateTime pFecIni, DateTime pFecFin)
        {
            try
            {
                List<Vehiculo> listaVehiculos = new List<Vehiculo>();
                listaVehiculos = PersistenciaUtilitarios.UtilitariosDisponibles(pFecIni, pFecFin);
                listaVehiculos.AddRange(PersistenciaAutos.AutosDisponibles(pFecIni, pFecFin));
                return listaVehiculos;
            }
            catch (Exception ex)
            { throw ex; }
        }
    }
}
