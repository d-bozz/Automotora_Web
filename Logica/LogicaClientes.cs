﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Persistencia;
using EntidadesCompartidas;

namespace Logica
{
    public class LogicaClientes
    {

        public static List<Cliente> ListarClientes()
        {
            try
            {
                return PersistenciaClientes.ListarClientes();
            }
            catch (Exception ex)
            { throw ex; }
        }

        public static Cliente Buscar(string pCedula)
        {
            Cliente c = PersistenciaClientes.Buscar(pCedula); 
            return c;
        }

        public static void Modificar(Cliente c)
        {
            
            PersistenciaClientes.Modificar((Cliente)c);
        }

        public static void Agregar(Cliente c)
        {
            
                PersistenciaClientes.Agregar((Cliente)c);
        }

        public static void Eliminar(Cliente c)
        {
            
                PersistenciaClientes.Eliminar(c.Cedula);           
        }

    }
}
