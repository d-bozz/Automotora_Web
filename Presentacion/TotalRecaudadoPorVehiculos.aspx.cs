﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntidadesCompartidas;
using Logica;

public partial class TotalRecaudadoPorVehiculos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                List<Vehiculo> listado = LogicaVehiculos.ListarVehiculos();
                cboVehiculos.DataTextField = "Datos";
                cboVehiculos.DataValueField = "Matricula";
                cboVehiculos.DataSource = listado;
                cboVehiculos.DataBind();
            }
        }
        catch (Exception  ex)
        { lblMensaje.Text = ex.Message; }
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try 
        {
            List<Alquiler> listado = LogicaAlquiler.AlquileresPorVehiculo(cboVehiculos.SelectedValue);
            GridView1.DataSource = listado;
            GridView1.DataBind();
            int total = 0;
            foreach (Alquiler a in listado)
            {
                total += a.Costo;
            }
            lblTotal.Text = "Total recaudado = u$s"+total;
            
        }
        catch (Exception ex)
        { lblMensaje.Text = ex.Message; }
    }
}