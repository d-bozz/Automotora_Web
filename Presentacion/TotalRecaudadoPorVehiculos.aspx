﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TotalRecaudadoPorVehiculos.aspx.cs" Inherits="TotalRecaudadoPorVehiculos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Damian Boz">

    <title>Concesionario</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/scrolling-nav.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../Presentacion/Default.aspx">Concesionario</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/MantenimientoClientes.aspx">Clientes</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/MantenimientoAutos.aspx">Autos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/MantenimientoUtilitarios.aspx">Utilitarios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/RealizarAlquiler.aspx">Alquiler</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/ListadoVehiculosDisponibles.aspx">Vehiculos Disponibles</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/TotalRecaudadoPorVehiculos.aspx">Total Recaudado</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>




<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            height: 30px;
        }
        .style2
        {
            height: 30px;
            text-align: right;
            width: 516px;
        }
        .style3
        {
            width: 393px;
        }
    </style>
</head>
<body bgcolor="SkyBlue">



    <form id="form1" runat="server">
    <div style="text-align: center">
    
        <br />
        <br />
        <br />
        <table align="right" style="width: 100%; margin-bottom: 0px; height: 63px;">
            <tr>
                <td class="style2">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="cboVehiculos" runat="server" 
                        
                        style="text-align: right" Height="60px" >
                    </asp:DropDownList>
                </td>
                <td class="style1" style="text-align: left">
                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    
        <br />
        <table style="width:100%;">
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td id="gvAlquileres">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" 
                        GridLines="None">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td style="text-align: left" align="left">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
                        onclick="btnBuscar_Click" style="font-weight: 700; text-align: left;" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <asp:Label ID="lblMensaje" runat="server" style="text-align: center"></asp:Label>
    
        <br />
    
    </div>
    </form>
</body>
</html>
