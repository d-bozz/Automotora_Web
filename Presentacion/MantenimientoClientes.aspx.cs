﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntidadesCompartidas;
using Logica;


public partial class MantenimientoClientes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            EstadoInicial();
            cldFechaNac.VisibleDate = DateTime.Today.AddYears(-25);
            cldFechaNac.SelectedDate = DateTime.Today.AddYears(-25);
        }
    }

    private void EstadoInicial()
    {
        btnAgregar.Enabled = false;
        BtnModificar.Enabled = false;
        btnEliminar.Enabled = false;

        btnBuscar.Enabled = true;

        txtCedula.Enabled = true;

        txtTarjeta.Enabled = false;
        txtNombre.Enabled = false;
        txtTelefono.Enabled = false;
        txtDireccion.Enabled = false;
        cldFechaNac.SelectedDate = DateTime.Today;
        cldFechaNac.VisibleDate = DateTime.Today;

        lblMensaje.Text = "";
        txtCedula.Text = "";
        txtTarjeta.Text = "";
        txtNombre.Text = "";
        txtTelefono.Text = "";
        txtDireccion.Text = "";
        
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            Cliente c = LogicaClientes.Buscar(txtCedula.Text);
            if (c == null)
            {
                lblMensaje.Text = ("No se ha encontrado un cliente con esa cedula.");
                HabilitarA();
            }
            else
            {
                HabilitarME();
                txtTarjeta.Text = c.Tarjeta.ToString();
                txtNombre.Text = c.Nombre;
                txtTelefono.Text = c.Telefono;
                txtDireccion.Text = c.Direccion;
                cldFechaNac.SelectedDate = c.FechaNac;
                cldFechaNac.VisibleDate = c.FechaNac;
             }
           
            
        }
        catch (Exception ex)
        { lblMensaje.Text = ex.Message; }
    }

    private void HabilitarA()
    {
        btnAgregar.Enabled = true;
        BtnModificar.Enabled = false;
        btnEliminar.Enabled = false;

        btnBuscar.Enabled = false;

        txtCedula.Enabled = false;

        txtNombre.Enabled = true;
        txtTarjeta.Enabled = true;
        txtDireccion.Enabled = true;
        txtTelefono.Enabled = true;
    }

    private void HabilitarME()
    {
        btnAgregar.Enabled = false;
        BtnModificar.Enabled = true;
        btnEliminar.Enabled = true;

        btnBuscar.Enabled = false;

        txtCedula.Enabled = false;

        txtNombre.Enabled = true;
        txtTarjeta.Enabled = true;
        txtDireccion.Enabled = true;
        txtTelefono.Enabled = true;
    }

    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        EstadoInicial();
        lblMensaje.Text = "";
  
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        try
        {
            int cedula = Convert.ToInt32(txtCedula.Text.Trim());
            if (cedula < 0)
                throw new Exception("La cedula no puede ser negativa.");
            if (cedula.ToString().Length != 8)
                throw new Exception("La cedula debe contener 8 caracteres");

            int tarjeta = Convert.ToInt32(txtTarjeta.Text);
            if (tarjeta < 0)
                throw new Exception("La tarjeta no puede ser negativa.");
            //if (tarjeta.ToString().Length == 16)
                //throw new Exception("La tarjeta debe contener 16 caracteres");

            string nombre = txtNombre.Text;
            if (string.IsNullOrEmpty(nombre))
                throw new Exception("El nombre no puede ser vacío.");

            string telefono = txtTelefono.Text;
            if (string.IsNullOrEmpty(telefono))
                throw new Exception("El telefono no puede ser vacío.");

            string direccion = txtDireccion.Text;
            if (string.IsNullOrEmpty(direccion))
                throw new Exception("La direccion no puede ser vacía.");

            if (cldFechaNac.SelectedDate > DateTime.Today)
                throw new Exception("La fecha debe ser menor a este dia.");


            Cliente c = new Cliente(txtNombre.Text, Convert.ToInt32(txtCedula.Text), Convert.ToInt32(txtTarjeta.Text), txtTelefono.Text, txtDireccion.Text, cldFechaNac.SelectedDate);

            LogicaClientes.Agregar(c);
            lblMensaje.Text = ("Se ha agregado correctamente el cliente con la cedula " + cedula);
        }
        catch (Exception ex)
        { lblMensaje.Text = ex.Message; }    



    }

    protected void BtnModificar_Click(object sender, EventArgs e)
    {
        try
        {
            int cedula = Convert.ToInt32(txtCedula.Text.Trim());
            if (cedula < 0)
                throw new Exception("La cedula no puede ser negativa.");
            if (cedula.ToString().Length != 8)
                throw new Exception("La cedula debe contener 8 caracteres");

            int tarjeta = Convert.ToInt32(txtTarjeta.Text);
            if (tarjeta < 0)
                throw new Exception("La tarjeta no puede ser negativa.");
            //if (tarjeta.ToString().Length != 16)
                //throw new Exception("La tarjeta debe contener 16 caracteres");

            string nombre = txtNombre.Text;
            if (string.IsNullOrEmpty(nombre))
                throw new Exception("El nombre no puede ser vacío.");

            string telefono = txtTelefono.Text;
            if (string.IsNullOrEmpty(telefono))
                throw new Exception("El telefono no puede ser vacío.");

            string direccion = txtDireccion.Text;
            if (string.IsNullOrEmpty(direccion))
                throw new Exception("La direccion no puede ser vacía.");


            if (cldFechaNac.SelectedDate > DateTime.Today)
                throw new Exception("La fecha debe ser menor a este dia.");


            Cliente c = new Cliente(txtNombre.Text,  Convert.ToInt32(txtCedula.Text),  Convert.ToInt32(txtTarjeta.Text), txtTelefono.Text, txtDireccion.Text, cldFechaNac.SelectedDate);

            LogicaClientes.Modificar(c);
            lblMensaje.Text = ("Se ha modificado correctamente el cliente con la cedula " + cedula);
        }
        catch (Exception ex)
        { lblMensaje.Text = ex.Message; }    



    }

    protected void btnBaja_Click(object sender, EventArgs e)
    {
        try
        {

            Cliente c = new Cliente("ejemplo", Convert.ToInt32(txtCedula.Text),12345678,"123456789", "ejemplo", cldFechaNac.SelectedDate);
            LogicaClientes.Eliminar(c);
            lblMensaje.Text = ("Se ha eliminado correctamente el cliente con cedula " + c.Cedula);
        }
        catch (Exception ex)
        { lblMensaje.Text = ex.Message; }
    }
    protected void calFin_SelectionChanged(object sender, EventArgs e)
    {

    }
}