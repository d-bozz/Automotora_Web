﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using EntidadesCompartidas;
using Logica;

public partial class ListadoVehiculosDisponibles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            EstadoInicial();
        }
    }

    protected void EstadoInicial()
    {

        calInicio.Enabled = true;
        calInicio.SelectedDate = DateTime.Now;
        calInicio.VisibleDate = DateTime.Now;
        calFin.Enabled = true;
        calFin.SelectedDate = DateTime.Now;
        calFin.VisibleDate = DateTime.Now;
        lblMensaje.Text = "Seleccione las fechas y pulse 'Consultar Disponibles'";
    }
    protected void btnListar_Click(object sender, EventArgs e)
    {
        try
        {
            List<Vehiculo> listaVehiculos = LogicaVehiculos.ListarDisponibles(calInicio.SelectedDate, calFin.SelectedDate);
            if (listaVehiculos.Count != 0)
            {
                lstVehiculos.DataSource = listaVehiculos;
                lstVehiculos.DataBind();
                lblMensaje.Text = "Listando vehiculos disponibles entre el: " + calInicio.SelectedDate.ToShortDateString() + " y el: " + calFin.SelectedDate.ToShortDateString();
            }
            if (listaVehiculos.Count == 0)
            {
                lstVehiculos.DataSource = "";
                lstVehiculos.DataBind();
                lblMensaje.Text = "No hay vehiculos disponibles para el periodo entre el: " + calInicio.SelectedDate.ToShortDateString() + " y el: " + calFin.SelectedDate.ToShortDateString();
            }
        }
        catch (Exception ex)
        { lblMensaje.Text = ex.Message; }
    }
    
}