﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MantenimientoClientes.aspx.cs" Inherits="MantenimientoClientes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Damian Boz">

    <title>Concesionario</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/scrolling-nav.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../Presentacion/Default.aspx">Concesionario</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/MantenimientoClientes.aspx">Clientes</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/MantenimientoAutos.aspx">Autos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/MantenimientoUtilitarios.aspx">Utilitarios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/RealizarAlquiler.aspx">Alquiler</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/ListadoVehiculosDisponibles.aspx">Vehiculos Disponibles</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/TotalRecaudadoPorVehiculos.aspx">Total Recaudado</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>






<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Mantenimiento Clientes</title>
    <style type="text/css">
        .style1
        {
            width: 123px;
            height: 35px;
        }
        .style2
        {
            width: 191px;
            height: 35px;
        }
        .style5
        {
            width: 123px;
            height: 21px;
        }
        .style6
        {
            width: 191px;
            height: 21px;
        }
        .style7
        {
            width: 123px;
            height: 40px;
        }
        .style8
        {
            width: 191px;
            height: 40px;
        }
        .style9
        {
            width: 123px;
            height: 42px;
        }
        .style10
        {
            width: 191px;
            height: 42px;
        }
        .style11
        {
            height: 107px;
        }
        .style12
        {
            width: 123px;
            height: 47px;
        }
        .style13
        {
            width: 191px;
            height: 47px;
        }
        .style14
        {
            width: 123px;
            height: 45px;
        }
        .style15
        {
            width: 191px;
            height: 45px;
        }
        .style16
        {
            height: 21px;
        }
        .style17
        {
            height: 35px;
        }
        .style18
        {
            height: 40px;
        }
        .style19
        {
            height: 47px;
        }
        .style20
        {
            height: 45px;
        }
        .style21
        {
            height: 42px;
        }
        .style22
        {
            height: 107px;
            width: 191px;
        }
    </style>
</head>
<body bgcolor="SkyBlue">
    <form id="form1" runat="server">
    <div style="text-align: center">
        <br />
        <b style="text-decoration: underline">
        <br />
        <br />
        </b>
        <table >
            <tr>
                <td class="style5">
                    Cedula:</td>
                <td class="style16">
                    <asp:TextBox ID="txtCedula" runat="server"></asp:TextBox>
                </td>
                <td class="style6">
                    <asp:Button ID="btnBuscar" runat="server" Font-Bold="True" 
                        Text="Buscar" OnClick="btnBuscar_Click" /></td>
            </tr>
            <tr>
                <td class="style1">
                    Tarjeta de credito:</td>
                <td class="style17">
                    <asp:TextBox ID="txtTarjeta" runat="server"></asp:TextBox></td>
                <td class="style2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style7">
                    Nombre Completo:</td>
                <td class="style18">
                    <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox></td>
                <td class="style8">
                </td>
            </tr>
            <tr>
                <td class="style12">
                    Telefono:</td>
                <td align="center" class="style19">
                    <asp:TextBox ID="txtTelefono" runat="server"></asp:TextBox></td>
                <td align="center" class="style13">
                </td>
            </tr>
            <tr>
             <td class="style14">
                    Dirección:</td>
                <td align="center" class="style20">
                    <asp:TextBox ID="txtDireccion" runat="server"></asp:TextBox></td>
                <td align="center" class="style15">
                </td>
            </tr>
            <tr>
             <td class="style9">
                    Fecha de Nacimiento:</td>
                <td align="center" class="style21">
                    &nbsp;&nbsp;&nbsp;
                    <asp:Calendar ID="cldFechaNac" runat="server" BackColor="White" 
                        BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" 
                        DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" 
                        ForeColor="#003399" Height="16px" Width="220px">
                        <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                        <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" 
                            Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                        <WeekendDayStyle BackColor="#CCCCFF" />
                    </asp:Calendar>
                </td>
                <td align="center" class="style10">
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="style11">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnAgregar" runat="server" Font-Bold="True" OnClick="btnAgregar_Click"
                        Text="Agregar" Width="74px" />
                    <asp:Button ID="BtnModificar" runat="server" Font-Bold="True" 
                        Text="Modificar" OnClick="BtnModificar_Click" Enabled="False" />
                    <asp:Button ID="btnEliminar" runat="server" Font-Bold="True" 
                        Text="Eliminar" OnClick="btnBaja_Click" Enabled="False" /></td>
                <td align="center" colspan="1" class="style22">
                    <asp:Button ID="btnLimpiar" runat="server" Font-Bold="True" OnClick="btnLimpiar_Click"
                        Text="Limpiar" Width="87px" />
                </td>
            </tr>
        </table>
        <br />
        <asp:Label ID="lblMensaje" runat="server"></asp:Label>&nbsp;<br />
        <br />
    
    </div>
    </form>
</body>
</html>

