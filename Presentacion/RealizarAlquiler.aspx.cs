﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Logica;
using EntidadesCompartidas;

public partial class RealizarAlquiler : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                List<Cliente> listaClientes;
                listaClientes = LogicaClientes.ListarClientes();
                if (listaClientes.Count != 0)
                {
                    cboCliente.DataSource = listaClientes;
                    cboCliente.DataTextField = "Datos";
                    cboCliente.DataValueField = "Cedula";
                    cboCliente.DataBind();
                }

                List<Vehiculo> listaVehiculos;
                listaVehiculos = LogicaVehiculos.ListarVehiculos();
                if (listaVehiculos.Count != 0)
                {
                    cboVehiculo.DataSource = listaVehiculos;
                    cboVehiculo.DataTextField = "Datos";
                    cboVehiculo.DataValueField = "Matricula";
                    cboVehiculo.DataBind();
                }
                if (listaClientes.Count == 0 || listaVehiculos.Count == 0)
                {
                    NoDisponible();
                    throw new Exception("Faltan datos para poder realizar Alquileres.");
                }
                EstadoInicial();
            }
            catch (Exception ex)
            { lblMensaje.Text = ex.Message; }
        }
    }

    protected void NoDisponible()
    {
        cboCliente.Enabled = false;
        cboVehiculo.Enabled = false;
        calInicio.Enabled = false;
        calInicio.SelectedDate = DateTime.Today;
        calInicio.VisibleDate = DateTime.Today;
        calFin.Enabled = false;
        calFin.SelectedDate = DateTime.Today;
        calFin.VisibleDate = DateTime.Today;
        btnCCosto.Enabled = false;
        btnConfirmar.Enabled = false;
        btnCancelar.Enabled = false;
    }

    protected void EstadoInicial()
    {
        cboCliente.Enabled = true;
        cboVehiculo.Enabled = true;
        calInicio.Enabled = true;
        calInicio.SelectedDate = DateTime.Today;
        calInicio.VisibleDate = DateTime.Today;
        calFin.Enabled = true;
        calFin.SelectedDate = DateTime.Today;
        calFin.VisibleDate = DateTime.Today;
        btnCCosto.Enabled = true;
        btnConfirmar.Enabled = false;
        btnCancelar.Enabled = false;
        lblCostoTotal.Text = "";
        lblMensaje.Text = "";
    }

    protected void ConfirmarAlquiler()
    {
        cboCliente.Enabled = false;
        cboVehiculo.Enabled = false;
        calInicio.Enabled = false;
        calFin.Enabled = false;
        btnCCosto.Enabled = false;
        btnConfirmar.Enabled = true;
        btnCancelar.Enabled = true;
    }

    protected void SoloCancelar()
    {
        cboCliente.Enabled = false;
        cboVehiculo.Enabled = false;
        calInicio.Enabled = false;
        calFin.Enabled = false;
        btnCCosto.Enabled = false;
        btnConfirmar.Enabled = false;
        btnCancelar.Enabled = true;
    }

    protected void btnCCosto_Click(object sender, EventArgs e)
    {
        try
        {
            if (calInicio.SelectedDate >= calFin.SelectedDate)
            {
                throw new Exception("La fecha minima de alquiler es de un dia.");
            }
            int costoTotal = 0;
            Cliente c = LogicaClientes.Buscar(cboCliente.SelectedValue);
            Vehiculo v = LogicaVehiculos.Buscar(cboVehiculo.SelectedValue);
            Alquiler a = new Alquiler(c, v, calInicio.SelectedDate, calFin.SelectedDate);
            costoTotal = a.Costo;
            lblMensaje.Text = "Pulse Confirmar para realizar el alquiler o Cancelar para salir.";
            lblCostoTotal.Text = costoTotal.ToString();
            ConfirmarAlquiler();
        }
        catch (Exception ex)
        {
            SoloCancelar();
            lblMensaje.Text = ex.Message; 
        }
    }


    protected void btnConfirmar_Click(object sender, EventArgs e)
    {
        try
        {
            if (calInicio.SelectedDate >= calFin.SelectedDate)
            {
                throw new Exception("La fecha minima de alquiler es de un dia.");
            }
            Cliente c = LogicaClientes.Buscar(cboCliente.SelectedValue);
            Vehiculo v = LogicaVehiculos.Buscar(cboVehiculo.SelectedValue);
            Alquiler a = new Alquiler(c, v, calInicio.SelectedDate, calFin.SelectedDate);
            LogicaAlquiler.RealizarAlquiler(a);
            lblMensaje.Text = "Alquiler realizado con éxito.";
        }
        catch (Exception ex)
        {
            SoloCancelar();
            lblMensaje.Text = ex.Message; 
        }
    }
    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        EstadoInicial();
    }
}