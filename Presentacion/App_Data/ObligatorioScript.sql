use master
go

--------------------------
if exists(select * from sysdatabases where name = 'Obligatorio')
begin
	drop database Obligatorio
end
go


--creo la base de datos
create database Obligatorio
go


--selecciono la base de datos
use Obligatorio
go


--creo tablas
create table Vehiculos
(
	letrasMatV varchar(3) not null,
	nrosMatV int not null,
	marcaV varchar(15) not null,
	modeloV varchar(25) not null,
	a�oV int not null,
	cantPV int not null,
	CostoDV int not null,
	primary key(letrasMatV, nrosMatV)
)
go

create table Autos
(
	letrasMatV varchar(3) not null ,
	nrosMatV int not null,
	anclajeA varchar(10) not null,
	primary key(letrasMatV, nrosMatV),
	foreign key (letrasMatV, nrosMatV) references Vehiculos(letrasMatV, nrosMatV)
)
go

create table Utilitarios
(
	letrasMatV varchar(3) not null,
	nrosMatV int not null,
	CapacidadU int not null,
	tipoU varchar(9) not null,
	primary key (letrasMatV, nrosMatV),
	foreign key (letrasMatV, nrosMatV) references Vehiculos(letrasMatV, nrosMatV)
	 
)
go

create table Clientes
(
	ciCli bigint not null primary key,
	TCCli bigint unique not null,
	nombreCli varchar(40) not null,
	dirCli varchar(30) not null,
	FNCli date not null,
	telCli varchar(10) not null
)
go

create table Alquileres
(
	codigoA int not null identity primary key,
	letrasMatV varchar(3) not null,
	nrosMatV int not null,
	foreign key (letrasMatV, nrosMatV) references Vehiculos(letrasMatV, nrosMatV),
	ciCli bigint not null foreign key references Clientes(ciCli),
	fechaIni date not null,
	fechaFin date not null
)
go

--datos de prueba
insert Vehiculos (letrasMatV,nrosMatV,CostoDV,a�oV,cantPV,marcaV,modeloV) values ('aaa',1111,100,1999,4,'fiat','600')
insert Vehiculos (letrasMatV,nrosMatV,CostoDV,a�oV,cantPV,marcaV,modeloV) values ('bbb',2222,115,2000,2,'fiat','fiorino')
insert Vehiculos (letrasMatV,nrosMatV,CostoDV,a�oV,cantPV,marcaV,modeloV) values ('fff',3333,115,2000,2,'chery','qq')
insert Vehiculos (letrasMatV,nrosMatV,CostoDV,a�oV,cantPV,marcaV,modeloV) values ('ddd',4444,115,2000,2,'citroen','berlingo')



insert Autos (letrasMatV,nrosMatV,anclajeA) values ('aaa',1111,'cinturon')
insert Autos (letrasMatV,nrosMatV,anclajeA) values ('fff',3333,'cinturon')

insert Utilitarios(letrasMatV,nrosMatV,CapacidadU,tipoU) values ('bbb',2222,500,'furgoneta')
insert Utilitarios(letrasMatV,nrosMatV,CapacidadU,tipoU) values ('ddd',4444,500,'furgoneta')



insert Clientes (ciCli,nombreCli,TCCli,FNCli,dirCli,telCli) values (10000001,'Pedro',11111,'19800125','yaruaron 1414',12345678)
insert Clientes (ciCli,nombreCli,TCCli,FNCli,dirCli,telCli) values (10000002,'Juan',22222,'19901005','18 de julio 1231',23456789)
insert Clientes (ciCli,nombreCli,TCCli,FNCli,dirCli,telCli) values (10000003,'Matias',33333,'19771022','Uruguay',34567891)
insert Clientes (ciCli,nombreCli,TCCli,FNCli,dirCli,telCli) values (10000004,'Ana',44444,'19881122','Paraguay',45678912)
insert Clientes (ciCli,nombreCli,TCCli,FNCli,dirCli,telCli) values (10000005,'Maria',55555,'19800125','Paysandy',56789123)
--insert Clientes (ciCli,nombreCli,TCCli,FNCli,dirCli,telCli) values (10000006,'Soy Menor',66666,'20010407','Rio Negro',67891234)


--ALQUILERES
INSERT INTO dbo.Alquileres (letrasMatV, nrosMatV, ciCli, fechaIni, fechaFin) VALUES ('aaa', 1111, 10000001, '2019-11-01', '2019-11-03')
INSERT INTO dbo.Alquileres (letrasMatV, nrosMatV, ciCli, fechaIni, fechaFin) VALUES ('bbb', 2222, 10000001, '2019-11-01', '2019-11-03')
INSERT INTO dbo.Alquileres (letrasMatV, nrosMatV, ciCli, fechaIni, fechaFin) VALUES ('aaa', 1111, 10000001, '2019-11-10', '2019-11-30')
INSERT INTO dbo.Alquileres (letrasMatV, nrosMatV, ciCli, fechaIni, fechaFin) VALUES ('bbb', 2222, 10000001, '2019-11-15', '2019-11-22')
INSERT INTO dbo.Alquileres (letrasMatV, nrosMatV, ciCli, fechaIni, fechaFin) VALUES ('fff', 3333, 10000001, '2018-01-01', '2018-01-10')
INSERT INTO dbo.Alquileres (letrasMatV, nrosMatV, ciCli, fechaIni, fechaFin) VALUES ('ddd', 4444, 10000001, '2018-02-05', '2018-03-05')


go
CREATE PROC ListarV
AS
BEGIN
SELECT * FROM Vehiculos
END



go
CREATE PROC ListarA
AS
BEGIN
SELECT V.letrasMatV, V.nrosMatV, marcaV, modeloV, a�oV, cantPV, CostoDV, anclajeA 
FROM Autos A JOIN Vehiculos V ON A.letrasMatV = V.letrasMatV AND A.nrosMatV = V.nrosMatV 
END



go
CREATE PROC ListarU
AS
BEGIN
SELECT V.letrasMatV, V.nrosMatV, marcaV, modeloV, a�oV, cantPV, CostoDV, CapacidadU, tipoU 
FROM Utilitarios U JOIN Vehiculos V ON U.letrasMatV = V.letrasMatV AND U.nrosMatV = V.nrosMatV 
END



go
CREATE PROC ListarC
AS
BEGIN
SELECT * FROM Clientes
END




go
CREATE PROC BuscarV
--ALTER PROC BuscarV
@letrasMatV varchar(3)
,@nrosMatV int
as
BEGIN
SELECT * FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV
END
--exec BuscarV 'aaa',1111

go
CREATE PROC BuscarA
--ALTER PROC BuscarA
@letrasMatV varchar(3)
,@nrosMatV int
as
BEGIN
SELECT V.letrasMatV, V.nrosMatV, marcaV, modeloV, a�oV, cantPV, CostoDV, anclajeA 
FROM Autos A JOIN Vehiculos V ON A.letrasMatV = V.letrasMatV AND A.nrosMatV = V.nrosMatV 
WHERE A.letrasMatV = @letrasMatV AND A.nrosMatV = @nrosMatV
END
--exec BuscarA 'aaa',1111

go
CREATE PROC BuscarU
--ALTER PROC BuscarU
@letrasMatV varchar(3)
,@nrosMatV int
as
BEGIN
SELECT V.letrasMatV, V.nrosMatV, marcaV, modeloV, a�oV, cantPV, CostoDV, CapacidadU, tipoU 
FROM Utilitarios U JOIN Vehiculos V ON U.letrasMatV = V.letrasMatV AND U.nrosMatV = V.nrosMatV 
WHERE U.letrasMatV = @letrasMatV AND U.nrosMatV = @nrosMatV
END
--exec BuscarU 'bbb',2222

go
CREATE PROC BuscarC
--ALTER PROC BuscarC
@ciCli bigint
as
BEGIN
SELECT ciCli, TCCli, nombreCli, dirCli, FNCli, telCli  FROM clientes WHERE ciCli = @ciCli
END


go
CREATE PROC EliminarA
--ALTER PROC EliminarA
@letrasMatV varchar(3)
,@nrosMatV int
as
BEGIN

IF NOT EXISTS (SELECT * FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
RETURN -1 --si no esta en la base

IF NOT EXISTS (SELECT * FROM Autos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
RETURN -2 --si esta en la base pero no como auto

IF EXISTS (SELECT letrasMatV, nrosMatV FROM Alquileres WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
RETURN -3 --tiene alquileres asociados

DECLARE @ERROR int
BEGIN TRAN

DELETE FROM Autos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV
SET @ERROR = @@ERROR

IF @ERROR <> 0 
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

DELETE FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV
SET @ERROR = @@ERROR

IF @ERROR <> 0 
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

COMMIT TRAN
RETURN 1

END

/*
DECLARE @return int
EXEC @return = EliminarA 'aaa',1111
print @return
select * from Vehiculos
*/




go
CREATE PROC EliminarU
--ALTER PROC EliminarU
@letrasMatV varchar(3)
,@nrosMatV int
as
BEGIN

IF NOT EXISTS (SELECT * FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
RETURN -1 --si no esta en la base

IF NOT EXISTS (SELECT * FROM Utilitarios WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
RETURN -2 --si esta en la base pero no como utilitario

IF EXISTS (SELECT letrasMatV, nrosMatV FROM Alquileres WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
RETURN -3 --tiene alquileres asociados

DECLARE @ERROR int

BEGIN TRAN
DELETE FROM Utilitarios WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV
SET @ERROR = @@ERROR

IF @ERROR <> 0 
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

DELETE FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV
SET @ERROR = @@ERROR

IF @ERROR <> 0 
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

COMMIT TRAN
RETURN 1

END

/*
DECLARE @return int
EXEC @return = EliminarU 'bbb',2222
print @return
select * from Vehiculos
*/




go
CREATE PROC ModificarA
--ALTER PROC ModificarA
@letrasMatV varchar(3)
,@nrosMatV int
,@anclajeA varchar(10)
,@marcaV varchar(15)
,@modeloV varchar(25)
,@a�oV int
,@cantPV int
,@CostoDV int
as
BEGIN

IF NOT EXISTS (SELECT * FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
RETURN -1 --si no esta en la base

IF NOT EXISTS (SELECT * FROM Autos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
RETURN -2 --si esta en la base pero no como auto

DECLARE @ERROR INT
BEGIN TRAN
UPDATE Autos SET anclajeA = @anclajeA WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV
SET @ERROR = @@ERROR

IF @ERROR <> 0
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

UPDATE Vehiculos SET CostoDV = @CostoDV, a�oV = @a�oV, cantPV = @cantPV, marcaV = @marcaV, modeloV = @modeloV
WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV
SET @ERROR = @@ERROR

IF @ERROR <> 0
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

COMMIT TRAN
RETURN 1						

END
/*
DECLARE @return int
EXEC @return = ModificarA 'aaa',1111,'latch','otro','alguno',45,1,333
PRINT @return
select * from Autos
select * from Vehiculos
*/




go
CREATE PROC ModificarU
--ALTER PROC ModificarU
@letrasMatV varchar(3)
,@nrosMatV int
,@CapacidadU int
,@tipoU varchar(9)
,@marcaV varchar(15)
,@modeloV varchar(25)
,@a�oV int
,@cantPV int
,@CostoDV int
as
BEGIN

IF NOT EXISTS (SELECT * FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
RETURN -1 --si no esta en la base

IF NOT EXISTS (SELECT * FROM Utilitarios WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
RETURN -2 --si esta en la base pero no como utilitario



DECLARE @ERROR INT
BEGIN TRAN
UPDATE Utilitarios SET tipoU = @tipoU, CapacidadU = @CapacidadU WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV
SET @ERROR = @@ERROR

IF @ERROR <> 0
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

UPDATE Vehiculos SET CostoDV = @CostoDV, a�oV = @a�oV, cantPV = @cantPV, marcaV = @marcaV, modeloV = @modeloV 
WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV
SET @ERROR = @@ERROR

IF @ERROR <> 0
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

COMMIT TRAN
RETURN 1						

END
/*
DECLARE @AUX int
EXEC @AUX = ModificarU 'bbb',2222,200,'pickup','otro','alguno',13,1,800
PRINT @AUX
SELECT * FROM Vehiculos 
SELECT * FROM Utilitarios
*/




go

CREATE PROC AgregarA
--ALTER PROC AgregarA
@letrasMatV varchar(3)
,@nrosMatV int
,@anclajeA varchar(10)
,@marcaV varchar(15)
,@modeloV varchar(25)
,@a�oV int
,@cantPV int
,@CostoDV int
as
BEGIN

IF EXISTS (SELECT * FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV )
RETURN -1 --si ESTA no lo puedo agregar

DECLARE @ERROR int
BEGIN TRAN
INSERT Vehiculos (letrasMatV,nrosMatV,CostoDV,a�oV,cantPV,marcaV,modeloV) VALUES 
(@letrasMatV,@nrosMatV,@CostoDV,@a�oV,@cantPV,@marcaV,@modeloV)

SET @ERROR = @@ERROR
IF @ERROR<>0
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

INSERT Autos (letrasMatV,nrosMatV,anclajeA) VALUES (@letrasMatV,@nrosMatV,@anclajeA)

SET @ERROR = @@ERROR
IF @ERROR<>0
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

COMMIT TRAN
RETURN 1

END

/*
DECLARE @AUX INT
EXEC @AUX = AgregarA 'ccc',3333,'isofix','cuanto','vale ese rato',2000,4,400
PRINT @AUX
*/



go
CREATE PROC AgregarU
--ALTER PROC AgregarU
@letrasMatV varchar(3)
,@nrosMatV int
,@CapacidadU int
,@tipoU varchar(9)
,@marcaV varchar(15)
,@modeloV varchar(25)
,@a�oV int
,@cantPV int
,@CostoDV int
as
BEGIN

IF EXISTS (SELECT * FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV )
RETURN -1 --si ESTA no lo puedo agregar

DECLARE @ERROR int
BEGIN TRAN
INSERT Vehiculos (letrasMatV,nrosMatV,CostoDV,a�oV,cantPV,marcaV,modeloV) VALUES 
(@letrasMatV,@nrosMatV,@CostoDV,@a�oV,@cantPV,@marcaV,@modeloV)

SET @ERROR = @@ERROR
IF @ERROR<>0
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

INSERT Utilitarios(letrasMatV,nrosMatV,CapacidadU,tipoU) VALUES (@letrasMatV,@nrosMatV,@CapacidadU,@tipoU)

SET @ERROR = @@ERROR
IF @ERROR<>0
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

COMMIT TRAN
RETURN 1

END
/*
DECLARE @AUX INT
EXEC @AUX = AgregarU 'ddd',4444,999,'pickup', 'toyota', 'hilux', 1998, 4, 700
PRINT @AUX
*/




go
CREATE PROC AgregarC
--ALTER PROC AgregarC
@ciCli bigint
,@TCCli bigint
,@nombreCli varchar(40)
,@dirCli varchar(30)
,@FNCli date
,@telCli varchar(10)
as
BEGIN

IF EXISTS (SELECT ciCli FROM Clientes WHERE ciCli = @ciCli)
RETURN -1 --ci repetida

IF EXISTS (SELECT TCCli FROM Clientes WHERE TCCli = @TCCli)
RETURN -2 --tarjeta repetida

DECLARE @ERROR int
INSERT Clientes (ciCli, TCCli,nombreCli,dirCli,FNCli,telCli) VALUES (@ciCli,@TCCli,@nombreCli,@dirCli,@FNCli,@telCli)
SET @ERROR = @@ERROR

IF @ERROR <> 0
RETURN -6 --ERROR SQL

RETURN 1

END
/*
DECLARE @AUX INT
EXEC @AUX = AgregarC 22222,1223,'GG','iokese','19990203',123456
PRINT @AUX
*/



go
CREATE PROC ModificarC
--ALTER PROC ModificarC
@ciCli bigint
,@TCCli bigint
,@nombreCli varchar(40)
,@dirCli varchar(30)
,@FNCli date
,@telCli varchar(10)
as
BEGIN

IF NOT EXISTS (SELECT ciCli FROM Clientes WHERE ciCli = @ciCli)
RETURN -1 --no esta el que quiero modificar

DECLARE @auxTC bigint
SELECT @auxTC = TCCli FROM Clientes WHERE ciCli = @ciCli
IF @auxTC <> @TCCli
BEGIN
	IF EXISTS (SELECT TCCli FROM Clientes WHERE TCCli = @TCCli)
	BEGIN
	RETURN -2 --tarjeta repetida
	END
END

DECLARE @ERROR int
UPDATE Clientes SET TCCli = @TCCli ,nombreCli = @nombreCli,dirCli = @dirCli,FNCli = @FNCli ,telCli = @telCli
WHERE ciCli = @ciCli
SET @ERROR = @@ERROR

IF @ERROR <> 0
RETURN -6 --ERROR SQL

RETURN 1

END
/*
DECLARE @AUX INT
EXEC @AUX = ModificarC 1111111,15223,'GG','iokese','19990203',123456
PRINT @AUX 
select * from Clientes
*/



go
CREATE PROC EliminarC
--ALTER PROC EliminarC
@ciCli bigint
as
BEGIN

IF NOT EXISTS (SELECT ciCli FROM Clientes WHERE ciCli = @ciCli)
RETURN -1 --no esta el que quiero eliminar

DECLARE @ERROR int
BEGIN TRAN

DELETE FROM Alquileres WHERE ciCli = @ciCli
SET @ERROR = @@ERROR
IF @ERROR <> 0 
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

DELETE FROM Clientes WHERE ciCli = @ciCli
SET @ERROR = @@ERROR
IF @ERROR <> 0
BEGIN
ROLLBACK TRAN
RETURN -6 --ERROR SQL
END

COMMIT TRAN
RETURN 1

END

/*
declare @aux int
exec @aux = EliminarC 1111111
print @aux
select * from Clientes
*/




go 
CREATE PROC RealizarA
--ALTER PROC RealizarA
@ciCli bigint
,@letrasMatV varchar(3)
,@nrosMatV int
,@fechaIni date
,@fechaFin date
as
BEGIN

DECLARE @dias int
SET @dias = datediff (dd,@fechaIni,@FechaFin)
IF @dias <1 
	RETURN -2  --No pueden haber alquileres que duren menos de un dia

DECLARE @EDAD INT
SET @EDAD = DATEDIFF(yy,(SELECT FNCli FROM Clientes WHERE ciCli = @ciCli),GETDATE())
IF @EDAD < 25
	RETURN -4 --Menor de 25

IF NOT EXISTS (SELECT * FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV)
	RETURN -1 --No esta el vehiculo que me pidieron

IF NOT EXISTS (SELECT * FROM Clientes WHERE ciCli = @ciCli)
	RETURN -3 --No esta el cliente

IF EXISTS (SELECT codigoA FROM Alquileres A 
			WHERE (@fechaIni <= a.fechafin)  and  (@fechaFin >= a.fechaIni)
			AND letrasMatV = @letrasMatV
			AND nrosMatV = @nrosMatV)
BEGIN
	RETURN -5 --No esta disponible
END

DECLARE @precio money
SELECT @precio = CostoDV FROM Vehiculos WHERE letrasMatV = @letrasMatV AND nrosMatV = @nrosMatV
SET @precio = @precio * @dias

DECLARE @ERROR int
INSERT Alquileres (ciCli,letrasMatV,nrosMatV,fechaIni,fechaFin) VALUES (@ciCli,@letrasMatV,@nrosMatV,@fechaIni,@fechaFin)
SET @ERROR = @@ERROR
IF @ERROR <> 0 
	RETURN -6 --ERROR SQL
RETURN 1
END

/*
Declare @retorno int
exec @retorno = RealizarA 1111111, 'aaa',1111,'20171101','20171103'
--exec @retorno = RealizarA 1111111, 'bbb',2222,'20171101','20171103'
--exec @retorno = RealizarA 1111111, 'aaa',1111,'20171110','20171130'
--exec @retorno = RealizarA 1111111, 'bbb',2222,'20171115','20171122'
PRINT @retorno
*/


go
CREATE PROC TotalVehiculo @letrasMatV varchar(3), @nrosMatV int 
--ALTER PROC TotalVehiculo @letrasMatV varchar(3), @nrosMatV int 
AS
BEGIN
SELECT a.codigoA,V.letrasMatV,v.nrosMatV ,fechaIni,fechaFin,ciCli
FROM Alquileres A JOIN Vehiculos V on (A.letrasMatV = V.letrasMatV AND A.nrosMatV = V.nrosMatV)
WHERE v.letrasMatV = @letrasMatV AND v.nrosMatV = @nrosMatV
group by a.codigoA,V.letrasMatV,V.nrosMatV,fechaIni,fechaFin,ciCli
END


go 
CREATE PROC AlquileresVehiculo 
--ALTER PROC AlquileresVehiculo
@nrosMatV int
,@letrasMatV varchar(3)
as
BEGIN

SELECT A.codigoA, A.ciCli, A.fechaIni, A.fechaFin FROM Alquileres A JOIN Vehiculos V 
ON (A.letrasMatV = V.letrasMatV AND A.nrosMatV = V.nrosMatV)
WHERE (A.letrasMatV = @letrasMatV AND A.nrosMatV = @nrosMatV)

END
go
--exec TotalVehiculo 'aaa',1111
--exec TotalVehiculo 'bbb',2222


/*
	marcaV varchar(15) not null,
	modeloV varchar(25) not null,
	a�oV int not null,
	cantPV int not null,
	CostoDV int not null,


*/


GO
CREATE proc UtilitariosDisp   
--ALTER proc UtilitariosDisp 
@INICIO date,
@FIN date  

AS
BEGIN
		SELECT DISTINCT e.letrasMatV, e.nrosMatV, marcaV, modeloV, a�oV, cantPV, CostoDV, CapacidadU, tipoU
		FROM Vehiculos E join Utilitarios u on u.letrasMatV = E.letrasMatV AND u.nrosMatV = E.nrosMatV
		WHERE NOT EXISTS (SELECT V.letrasMatV, v.nrosMatV 
							 FROM Vehiculos V LEFT JOIN Alquileres A 
							 on (V.letrasMatV = a.letrasMatV and v.nrosMatV = a.nrosMatV)
							 WHERE (@INICIO <= a.fechafin)  and  (@FIN >= a.fechaIni) and (v.letrasMatV = e.letrasMatV and v.nrosMatV = e.nrosMatV))
END
GO

GO
CREATE proc AutosDisp   
--ALTER proc AutosDisp 
@INICIO date,
@FIN date  

AS
BEGIN
		SELECT DISTINCT e.letrasMatV, e.nrosMatV, marcaV, modeloV, a�oV, cantPV, CostoDV, anclajeA 
		FROM Vehiculos E join Autos a on a.letrasMatV = E.letrasMatV AND a.nrosMatV = E.nrosMatV
		WHERE NOT EXISTS (SELECT V.letrasMatV, v.nrosMatV 
							 FROM Vehiculos V LEFT JOIN Alquileres A 
							 on (V.letrasMatV = a.letrasMatV and v.nrosMatV = a.nrosMatV)
							 WHERE (@INICIO <= a.fechafin)  and  (@FIN >= a.fechaIni) and (v.letrasMatV = e.letrasMatV and v.nrosMatV = e.nrosMatV))
END
GO

--exec UtilitariosDisp '2017-11-01', '2017-11-03' 
--exec AutosDisp '2017-11-01', '2017-11-03'




--SELECT * FROM Clientes
--SELECT * FROM Alquileres
--SELECT * FROM Vehiculos
--SELECT * FROM Utilitarios


						 