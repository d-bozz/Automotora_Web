﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MantenimientoAutos.aspx.cs" Inherits="MantenimientoAutos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Damian Boz">

    <title>Concesionario</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/scrolling-nav.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../Presentacion/Default.aspx">Concesionario</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/MantenimientoClientes.aspx">Clientes</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/MantenimientoAutos.aspx">Autos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/MantenimientoUtilitarios.aspx">Utilitarios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/RealizarAlquiler.aspx">Alquiler</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/ListadoVehiculosDisponibles.aspx">Vehiculos Disponibles</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Presentacion/TotalRecaudadoPorVehiculos.aspx">Total Recaudado</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>





<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            text-align: center;
        }
        .style2
        {
            text-align: center;
            width: 200px;
        }
        .style3
        {
            width: 200px;
        }
        .style4
        {
            text-align: center;
            width: 471px;
        }
        .style5
        {
            width: 471px;
        }
    </style>
</head>
<body bgcolor="SkyBlue">
    <form id="form1" runat="server">
    <div>
    
        <table style="width:100%; margin-top: 67px;">
            <tr>
                <td class="style1" colspan="3">
                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>
                    </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label1" runat="server" Text="Matricula"></asp:Label>
                </td>
                <td class="style4">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtMatricula" runat="server" style="text-align: center"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnBuscar" runat="server" onclick="btnBuscar_Click" 
                        Text="Buscar" style="font-weight: 700; text-align: right;" />
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label2" runat="server" Text="Marca"></asp:Label>
                </td>
                <td class="style4">
                    <asp:TextBox ID="txtMarca" runat="server"></asp:TextBox>
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label3" runat="server" Text="Modelo"></asp:Label>
                </td>
                <td style="text-align: center" class="style5">
                    <asp:TextBox ID="txtModelo" runat="server"></asp:TextBox>
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label6" runat="server" Text="Anio"></asp:Label>
                </td>
                <td class="style4">
                    <asp:TextBox ID="txtAño" runat="server"></asp:TextBox>
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label4" runat="server" Text="Cant. Puertas"></asp:Label>
                </td>
                <td class="style4">
                    <asp:TextBox ID="txtCantPuertas" runat="server"></asp:TextBox>
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label5" runat="server" Text="Tipo Anclaje"></asp:Label>
                </td>
                <td class="style4">
                    <asp:TextBox ID="txtTipoA" runat="server"></asp:TextBox>
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label7" runat="server" Text="Costo Alquiler"></asp:Label>
                </td>
                <td class="style4">
                    <asp:TextBox ID="txtCostoAlquiler" runat="server"></asp:TextBox>
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td class="style4">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnAgregar" runat="server" Text="Agregar" 
                        onclick="btnAgregar_Click" style="font-weight: 700" />
                    <asp:Button ID="btnModificar" runat="server" Text="Modificar" 
                        onclick="btnModificar_Click" style="font-weight: 700" />
                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" 
                        onclick="btnEliminar_Click" style="font-weight: 700" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnLimpiar" runat="server" onclick="btnLimpiar_Click" 
                        Text="Limpiar" style="font-weight: 700" Width="93px" />
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td class="style5">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right">
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    <p>
        &nbsp;</p>
    </form>
</body>
</html>
