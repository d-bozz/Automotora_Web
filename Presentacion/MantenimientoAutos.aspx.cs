﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntidadesCompartidas;
using Logica;

public partial class MantenimientoAutos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            EstadoInicial();
    }
    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        try
        {
            Vehiculo v = LogicaVehiculos.Buscar(txtMatricula.Text);
            if (v == null)
            {
                lblMensaje.Text = ("No se ha encontrado un Vehiculo con esa matricula.");
                HabilitarA();
            }
            else if (v is Auto)
            {
                HabilitarME();
                txtAño.Text = v.Año.ToString();
                txtCantPuertas.Text = v.CantPuertas.ToString();
                txtCostoAlquiler.Text = v.CostoAlquiler.ToString();
                txtMarca.Text = v.Marca;
                txtModelo.Text = v.Modelo;
                txtTipoA.Text = ((Auto)v).TipoA;
                lblMensaje.Text=("Se ha encontrado el auto con matricula "+v.Matricula);
            }
            else
                lblMensaje.Text=("La matricula "+v.Matricula+" corresponde a un UTILITARIO");
            
        }
        catch (Exception ex)
        { lblMensaje.Text = ex.Message; }
    }

    private void HabilitarA()
    {
        btnAgregar.Enabled = true;
        btnModificar.Enabled = false;
        btnEliminar.Enabled = false;

        btnBuscar.Enabled = false;

        txtMatricula.Enabled = false;

        txtCantPuertas.Enabled = true;
        txtCostoAlquiler.Enabled = true;
        txtMarca.Enabled = true;
        txtModelo.Enabled = true;
        txtTipoA.Enabled = true;
        txtAño.Enabled = true;
    }

    private void HabilitarME()
    {
        btnAgregar.Enabled = false;
        btnModificar.Enabled = true;
        btnEliminar.Enabled = true;

        btnBuscar.Enabled = false;

        txtMatricula.Enabled = false;

        txtCantPuertas.Enabled = true;
        txtCostoAlquiler.Enabled = true;
        txtMarca.Enabled = true;
        txtModelo.Enabled = true;
        txtTipoA.Enabled = true;
        txtAño.Enabled = true;
    }

    private void EstadoInicial()
    {
        btnAgregar.Enabled = false;
        btnModificar.Enabled = false;
        btnEliminar.Enabled = false;

        btnBuscar.Enabled = true;

        txtMatricula.Enabled = true;

        txtCantPuertas.Enabled = false;
        txtCostoAlquiler.Enabled = false;
        txtMarca.Enabled = false;
        txtModelo.Enabled = false;
        txtTipoA.Enabled = false;
        txtAño.Enabled = false;

        lblMensaje.Text = "";
        txtMatricula.Text = "";
        txtAño.Text = "";
        txtCantPuertas.Text = "";
        txtCostoAlquiler.Text = "";
        txtMarca.Text = "";
        txtModelo.Text = "";
        txtTipoA.Text = "";
    }
    
    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        EstadoInicial();
        lblMensaje.Text = "";
    }
    
    protected void btnModificar_Click(object sender, EventArgs e)
    {
        try
        {
            string matricula = txtMatricula.Text.Trim();
            if (string.IsNullOrEmpty(matricula))
                throw new Exception("La matricula no puede ser vacio.");
            if (matricula.Length != 7)
                throw new Exception("La matricula debe consistir de 7 caracteres (3 letras y 4 numeros)");
            if (!char.IsLetter(matricula, 0) || !char.IsLetter(matricula, 1) || !char.IsLetter(matricula, 2))
                throw new Exception("Los primeros tres caracteres de la matricula deben ser letras.");
            int aux = -1;
            if ( !int.TryParse(matricula.Substring(3,4), out aux))
                throw new Exception("Los ultimos cuatro caracteres de la matricula deben ser numeros.");            
            if(string.IsNullOrEmpty(txtAño.Text))
                throw new Exception("El año no puede ser vacio.");
            if (Convert.ToInt32(txtAño.Text) <= 0)
                throw new Exception("El año debe ser positivo");
            if (string.IsNullOrEmpty(txtCantPuertas.Text))
                throw new Exception("La cantidad de puertas no puede ser vacio.");
            if (Convert.ToInt32(txtCantPuertas.Text) <= 0)
                throw new Exception("La cantidad de puertas debe ser positiva");
            if (string.IsNullOrEmpty(txtCostoAlquiler.Text))
                throw new Exception("El costo del alquiler no puede ser vacio.");
            if (Convert.ToInt32(txtCostoAlquiler.Text) <= 0)
                throw new Exception("El costo del alquiler debe ser positivo");
            if (string.IsNullOrEmpty(txtMarca.Text))
                throw new Exception("La marca no puede ser vacio.");
            if (string.IsNullOrEmpty(txtModelo.Text))
                throw new Exception("El modelo no puede ser vacio.");
            if (string.IsNullOrEmpty(txtTipoA.Text))
                throw new Exception("La tipo de anclaje no puede ser vacio.");
            
            Auto a = new Auto(matricula, txtMarca.Text, txtModelo.Text, Convert.ToInt32(txtAño.Text), Convert.ToInt32(txtCantPuertas.Text),
                              Convert.ToInt32(txtCostoAlquiler.Text), txtTipoA.Text);
            LogicaVehiculos.Modificar(a);
            lblMensaje.Text= ("Se ha modificado correctamente el auto con la matricula "+matricula);
        }
        catch (Exception ex)
        { lblMensaje.Text = ex.Message; }
    }
    
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        try
        {
            string matricula = txtMatricula.Text.Trim();
            if (string.IsNullOrEmpty(matricula))
                throw new Exception("La matricula no puede ser vacio.");
            if (matricula.Length != 7)
                throw new Exception("La matricula debe consistir de 7 caracteres (3 letras y 4 numeros)");
            if (!char.IsLetter(matricula, 0) || !char.IsLetter(matricula, 1) || !char.IsLetter(matricula, 2))
                throw new Exception("Los primeros tres caracteres de la matricula deben ser letras.");
            int aux = -1;
            if (!int.TryParse(matricula.Substring(3, 4), out aux))
                throw new Exception("Los ultimos cuatro caracteres de la matricula deben ser numeros.");
            if (string.IsNullOrEmpty(txtAño.Text))
                throw new Exception("El año no puede ser vacio.");
            if (Convert.ToInt32(txtAño.Text) <= 0)
                throw new Exception("El año debe ser positivo");
            if (string.IsNullOrEmpty(txtCantPuertas.Text))
                throw new Exception("La cantidad de puertas no puede ser vacio.");
            if (Convert.ToInt32(txtCantPuertas.Text) <= 0)
                throw new Exception("La cantidad de puertas debe ser positiva");
            if (string.IsNullOrEmpty(txtCostoAlquiler.Text))
                throw new Exception("El costo del alquiler no puede ser vacio.");
            if (Convert.ToInt32(txtCostoAlquiler.Text) <= 0)
                throw new Exception("El costo del alquiler debe ser positivo");
            if (string.IsNullOrEmpty(txtMarca.Text))
                throw new Exception("La marca no puede ser vacio.");
            if (string.IsNullOrEmpty(txtModelo.Text))
                throw new Exception("El modelo no puede ser vacio.");
            if (string.IsNullOrEmpty(txtTipoA.Text))
                throw new Exception("La tipo de anclaje no puede ser vacio.");

            Auto a = new Auto(matricula, txtMarca.Text, txtModelo.Text, Convert.ToInt32(txtAño.Text), Convert.ToInt32(txtCantPuertas.Text),
                              Convert.ToInt32(txtCostoAlquiler.Text), txtTipoA.Text);
            LogicaVehiculos.Agregar(a);
            lblMensaje.Text = ("Se ha agregado correctamente el auto con la matricula " + matricula);
        }
        catch (Exception ex)
        { lblMensaje.Text = ex.Message; }
    }
    
    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        try
        {
            Auto a = new Auto(txtMatricula.Text, "ejemplo", "ejemplo", 1981, 4, 100, "cinturon");
            LogicaVehiculos.Eliminar(a);
            lblMensaje.Text=("Se ha eliminado correctamente el vehiculo con matricula "+a.Matricula);
        }
        catch (Exception ex)
        { lblMensaje.Text = ex.Message; }
    }
}