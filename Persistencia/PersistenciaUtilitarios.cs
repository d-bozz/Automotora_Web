﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using EntidadesCompartidas;


namespace Persistencia
{
    public class PersistenciaUtilitarios
    {

        public static List<Vehiculo> ListarUtilitarios()
        {

            List<Vehiculo> listaVehiculos = new List<Vehiculo>();
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                Utilitario u = null;
                SqlCommand cmd = new SqlCommand("ListarU", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cnn.Open();
                SqlDataReader lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    u = new Utilitario((string)lector[0] + lector[1].ToString(), (string)lector[2], (string)lector[3], (int)lector[4], (int)lector[5], (int)lector[6], (int)lector[7], (string)lector[8]);
                    listaVehiculos.Add(u);
                }
                lector.Close();
                return listaVehiculos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { cnn.Close(); }
        }

        public static List<Vehiculo> UtilitariosDisponibles(DateTime pFecIni, DateTime pFecFin)
        {
            List<Vehiculo> listadoDisponibles = new List<Vehiculo>();
            Utilitario u = null;
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            SqlCommand cmd = new SqlCommand("UtilitariosDisp", cnn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@INICIO", pFecIni);
            cmd.Parameters.AddWithValue("@FIN", pFecFin);
            try
            {
                cnn.Open();
                SqlDataReader lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    u = new Utilitario((string)lector[0] + lector[1].ToString(), (string)lector[2], (string)lector[3], (int)lector[4], (int)lector[5], (int)lector[6], (int)lector[7], (string)lector[8]);
                    listadoDisponibles.Add(u);
                }
                lector.Close();
                return listadoDisponibles;
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

         public static Utilitario Buscar(string pMatricula)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                Utilitario u = null;
                SqlCommand cmd = new SqlCommand("BuscarU", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@letrasMatV", pMatricula.Substring(0, 3));
                cmd.Parameters.AddWithValue("@nrosMatV", pMatricula.Substring(3, 4));
                cnn.Open();
                SqlDataReader lector = cmd.ExecuteReader();
                if (lector.Read())
                {
                    // V.letrasMatV, V.nrosMatV, marcaV, modeloV, añoV, cantPV, CostoDV, capacidad, tipoU
                    //V.letrasMatV, V.nrosMatV, marcaV, modeloV, añoV, cantPV, CostoDV, CapacidadU, tipoU 
                    string matricula = (string)lector[0];
                    matricula += lector[1].ToString();
                    string marca = (string)lector[2];
                    string modelo = lector[3].ToString();
                    int anio = (int)lector[4];
                    int cantPuertas = (int)lector[5];
                    int CostoAlquiler = (int)lector[6];
                    int capacidadcarga = (int)lector [7];
                    string TipoU = (string)lector[8];

                    u = new Utilitario(matricula, marca, modelo, anio, cantPuertas, CostoAlquiler, capacidadcarga, TipoU);
                }
                return u;
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

        public static void Modificar(Utilitario u)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("ModificarU", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@letrasMatV", u.Matricula.Substring(0, 3));
                cmd.Parameters.AddWithValue("@nrosMatV", u.Matricula.Substring(3, 4));
                cmd.Parameters.AddWithValue("@CapacidadU", u.CapCarga);
                cmd.Parameters.AddWithValue("@marcaV", u.Marca);
                cmd.Parameters.AddWithValue("@modeloV", u.Modelo);
                cmd.Parameters.AddWithValue("@añoV", u.Año);
                cmd.Parameters.AddWithValue("@cantPV", u.CantPuertas);
                cmd.Parameters.AddWithValue("@CostoDV", u.CostoAlquiler);
                cmd.Parameters.AddWithValue("@tipoU",u.Tipo);

                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);
                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == -1)
                    throw new Exception("No se existe un Utilitario con la matricula " + u.Matricula);
                if (valorRetorno == -2)
                    throw new Exception("La matricula " + u.Matricula + " corresponde a un Auto");
                if (valorRetorno == -6)
                    throw new Exception("Error de SQL");
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

        public static void Agregar(Utilitario u)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
/* ALTER PROC AgregarU
@letrasMatV varchar(3)
,@nrosMatV int
,@CapacidadU int
,@tipoU varchar(9)
,@marcaV varchar(15)
,@modeloV varchar(25)
,@añoV int
,@cantPV int
,@CostoDV int*/
                SqlCommand cmd = new SqlCommand("AgregarU", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@letrasMatV", u.Matricula.Substring(0, 3));
                cmd.Parameters.AddWithValue("@nrosMatV", u.Matricula.Substring(3, 4));
                cmd.Parameters.AddWithValue("@CapacidadU", u.CapCarga);
                cmd.Parameters.AddWithValue("@marcaV", u.Marca);
                cmd.Parameters.AddWithValue("@modeloV", u.Modelo);
                cmd.Parameters.AddWithValue("@añoV", u.Año);
                cmd.Parameters.AddWithValue("@cantPV", u.CantPuertas);
                cmd.Parameters.AddWithValue("@CostoDV", u.CostoAlquiler);
                cmd.Parameters.AddWithValue("@tipoU",u.Tipo);

                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);
                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == -1)
                    throw new Exception("No se existe un Utilitario con la matricula " + u.Marca);
                if (valorRetorno == -6)
                    throw new Exception("Error de SQL");
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

        public static void Eliminar(string pMatricula)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("EliminarU", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@letrasMatV", pMatricula.Substring(0, 3));
                cmd.Parameters.AddWithValue("@nrosMatV", pMatricula.Substring(3, 4));
                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);
                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == -1)
                    throw new Exception("No se existe un Uyilitario con la matricula " + pMatricula);
                if (valorRetorno == -2)
                    throw new Exception("La matricula " + pMatricula + " corresponde a un AUTO");
                if (valorRetorno == -3)
                    throw new Exception("No se puede eliminar el utilitario de matricula "+pMatricula+" porque tiene alquileres asociados.");
                if (valorRetorno == -6)
                    throw new Exception("Error de SQL");

            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }
     
    }
}
