﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using EntidadesCompartidas;


namespace Persistencia
{
    public class PersistenciaAlquiler
    {
        
        public static void RealizarAlquiler(Alquiler a)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("RealizarA", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ciCli", a.Cliente.Cedula);
                cmd.Parameters.AddWithValue("@letrasMatV", a.Vehiculo.Matricula.ToString().Substring(0, 3));
                cmd.Parameters.AddWithValue("@nrosMatV", a.Vehiculo.Matricula.ToString().Substring(3, 4));
                cmd.Parameters.AddWithValue("@fechaIni", a.FechaInicio);
                cmd.Parameters.AddWithValue("@fechaFin", a.FechaFin);
                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);

                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == -2)
                    throw new Exception("No pueden haber alquileres que duren menos de un dia.");
                if (valorRetorno == -4)
                    throw new Exception("No se puede realizar un alquiler a Clientes menores de 25.");
                if (valorRetorno == -1)
                    throw new Exception("No existe el vehiculo ingresado.");
                if (valorRetorno == -3)
                    throw new Exception("No existe el cliente ingresado.");
                if (valorRetorno == -5)
                    throw new Exception("Lo siento, el vehiculo seleccionado ya se encuentra alquilado en esas fechas.");
                if (valorRetorno == -6)
                    throw new Exception("Error SQL, contacte con el Administrador del sistema.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { cnn.Close(); }
        }

        public static List<Alquiler> AlquileresPorVehiculo(string pMatricula)
        {
            /*ALTER PROC TotalVehiculo @letrasMatV varchar(3), @nrosMatV int 
            AS
            BEGIN
            SELECT */
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                List<Alquiler> listado = new List<Alquiler>();
                Alquiler a = null;
                SqlCommand cmd = new SqlCommand("TotalVehiculo", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@letrasMatV", pMatricula.Substring(0, 3));
                cmd.Parameters.AddWithValue("@nrosMatV", pMatricula.Substring(3, 4));
                cnn.Open();
                SqlDataReader lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    //a.codigoA,V.letrasMatV,v.nrosMatV
                    //sum (v.CostoDV * DATEDIFF(dd,a.fechaIni,a.fechaFin)) AS 'Costo total',
                    //fechaIni,fechaFin,ciCli
                    int codigo = (int)lector[0];
                    string matricula = (string)lector[1];
                    matricula += (string)lector[2].ToString();
                    DateTime inicio = (DateTime)lector[3];
                    DateTime fin = (DateTime)lector[4];
                    string ciCli = (string)lector[5].ToString();
                    Cliente c = PersistenciaClientes.Buscar(ciCli);
                    Vehiculo v = PersistenciaAutos.Buscar(matricula);
                    if (v == null)
                        v = PersistenciaUtilitarios.Buscar(matricula);
                    a = new Alquiler(c, v, inicio, fin);
                    a.Codigo = codigo;
                    listado.Add(a);
                }
                return listado;
            }
            catch (Exception ex)
            { throw ex; }
            finally
            {
                cnn.Close();
            }
        }
    }
}
