﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using EntidadesCompartidas;

namespace Persistencia
{
    public class PersistenciaAutos
    {
        public static List<Vehiculo> AutosDisponibles(DateTime pFecIni, DateTime pFecFin)
        {
            List<Vehiculo> listadoDisponibles = new List<Vehiculo>();
            Auto a = null;
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            SqlCommand cmd = new SqlCommand("AutosDisp", cnn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@INICIO", pFecIni);
            cmd.Parameters.AddWithValue("@FIN", pFecFin);
            try
            {
                cnn.Open();
                SqlDataReader lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    a = new Auto((string)lector[0] + lector[1].ToString(), (string)lector[2], (string)lector[3], (int)lector[4], (int)lector[5], (int)lector[6], (string)lector[7]);
                    listadoDisponibles.Add(a);
                }
                lector.Close();
                return listadoDisponibles;
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

        public static List<Vehiculo> ListarAutos()
        {
            List<Vehiculo> listaVehiculos = new List<Vehiculo>();
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                Auto a = null;
                SqlCommand cmd = new SqlCommand("ListarA", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cnn.Open();
                SqlDataReader lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    a = new Auto((string)lector[0] + lector[1].ToString(), (string)lector[2], (string)lector[3], (int)lector[4], (int)lector[5], (int)lector[6], (string)lector[7]);
                    listaVehiculos.Add(a);
                }
                lector.Close();
                return listaVehiculos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { cnn.Close(); }
        }

        public static Auto Buscar(string pMatricula)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                Auto a = null;
                SqlCommand cmd = new SqlCommand("BuscarA", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@letrasMatV", pMatricula.Substring(0, 3));
                cmd.Parameters.AddWithValue("@nrosMatV", pMatricula.Substring(3, 4));
                cnn.Open();
                SqlDataReader lector = cmd.ExecuteReader();
                if (lector.Read())
                {
                    // V.letrasMatV, V.nrosMatV, marcaV, modeloV, añoV, cantPV, CostoDV, anclajeA 
                    string matricula = (string)lector[0];
                    matricula += lector[1].ToString();
                    string marca = (string)lector[2];
                    string modelo = lector[3].ToString();
                    int anio = (int)lector[4];
                    int cantPuertas = (int)lector[5];
                    int CostoAlquiler = (int)lector[6];
                    string TipoA = (string)lector[7];
                    a = new Auto(matricula, marca, modelo, anio, cantPuertas, CostoAlquiler, TipoA);
                }
                return a;
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

        public static void Modificar(Auto a)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("ModificarA", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@letrasMatV", a.Matricula.Substring(0, 3));
                cmd.Parameters.AddWithValue("@nrosMatV", a.Matricula.Substring(3, 4));
                cmd.Parameters.AddWithValue("@anclajeA", a.TipoA);
                cmd.Parameters.AddWithValue("@marcaV", a.Marca);
                cmd.Parameters.AddWithValue("@modeloV", a.Modelo);
                cmd.Parameters.AddWithValue("@añoV", a.Año);
                cmd.Parameters.AddWithValue("@cantPV", a.CantPuertas);
                cmd.Parameters.AddWithValue("@CostoDV", a.CostoAlquiler);
                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);
                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == -1)
                    throw new Exception("No se existe auto con la matricula " + a.Matricula);
                if (valorRetorno == -2)
                    throw new Exception("La matricula " + a.Matricula + " corresponde a un UTILITARIO");
                if (valorRetorno == -6)
                    throw new Exception("Error de SQL");
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

        public static void Agregar(Auto a)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("AgregarA", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@letrasMatV", a.Matricula.Substring(0, 3));
                cmd.Parameters.AddWithValue("@nrosMatV", a.Matricula.Substring(3, 4));
                cmd.Parameters.AddWithValue("@anclajeA", a.TipoA);
                cmd.Parameters.AddWithValue("@marcaV", a.Marca);
                cmd.Parameters.AddWithValue("@modeloV", a.Modelo);
                cmd.Parameters.AddWithValue("@añoV", a.Año);
                cmd.Parameters.AddWithValue("@cantPV", a.CantPuertas);
                cmd.Parameters.AddWithValue("@CostoDV", a.CostoAlquiler);
                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);
                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == -1)
                    throw new Exception("No se existe auto con la matricula " + a.Marca);
                if (valorRetorno == -6)
                    throw new Exception("Error de SQL");
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

        public static void Eliminar(string pMatricula)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("EliminarA", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@letrasMatV", pMatricula.Substring(0, 3));
                cmd.Parameters.AddWithValue("@nrosMatV", pMatricula.Substring(3, 4));
                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);
                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == -1)
                    throw new Exception("No se existe auto con la matricula " + pMatricula);
                if (valorRetorno == -2)
                    throw new Exception("La matricula " + pMatricula + " corresponde a un UTILITARIO");
                if (valorRetorno == -3)
                    throw new Exception("No se puede eliminar el auto de matricula "+pMatricula+" porque tiene alquileres asociados.");
                if (valorRetorno == -6)
                    throw new Exception("Error de SQL");

            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

    }
}
