﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using EntidadesCompartidas;

namespace Persistencia
{
    public class PersistenciaClientes
    {

        public static List<Cliente> ListarClientes()
        {
            List<Cliente> listaClientes = new List<Cliente>();
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("ListarC", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cnn.Open();
                SqlDataReader lector = cmd.ExecuteReader();
                while (lector.Read())
                {
                    Cliente c = new Cliente((string)lector[2], Convert.ToInt32(lector[0]), Convert.ToInt32(lector[1]), (string)lector[5], (string)lector[3], (DateTime)lector[4]);
                    listaClientes.Add(c);
                }
                return listaClientes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { cnn.Close(); }
        }

        public static Cliente Buscar(string pCedula)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                Cliente c = null;
                SqlCommand cmd = new SqlCommand("BuscarC", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ciCli", pCedula);
                cnn.Open();
                SqlDataReader lector = cmd.ExecuteReader();
                if (lector.Read())
                {
                    //ciCli, TCCli, nombreCli, dirCli, FNCli, telCli
                    int cedula = Convert.ToInt32( lector[0].ToString());
                    int tarjeta = Convert.ToInt32( lector[1].ToString());
                    string nombre = (string)lector[2];
                    string direccion = (string)lector[3];
                    DateTime fecha = (DateTime)lector[4];
                    string telefono = (string)lector[5];

                    c = new Cliente(nombre, cedula, tarjeta, telefono, direccion, fecha);
                }
                return c;
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

        public static void Modificar(Cliente c)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("ModificarC", cnn);
                cmd.CommandType = CommandType.StoredProcedure;

                //Agrego los parámetros con sus valores
                cmd.Parameters.AddWithValue("@ciCli", c.Cedula);
                cmd.Parameters.AddWithValue("@TCCli", c.Tarjeta);
                cmd.Parameters.AddWithValue("@nombreCli", c.Nombre);
                cmd.Parameters.AddWithValue("@dirCli", c.Direccion);
                cmd.Parameters.AddWithValue("@FNCli", c.FechaNac);
                cmd.Parameters.AddWithValue("@telCli", c.Telefono);

                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);
                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == -1)
                    throw new Exception("No se existe un cliente con l cedula " + c.Cedula);
                if (valorRetorno == -2)
                    throw new Exception("La tarjeta " + c.Tarjeta + " ya se encuentra repetida.");
                if (valorRetorno == -6)
                    throw new Exception("Error de SQL");
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

        public static void Agregar(Cliente c)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("AgregarC", cnn);
                cmd.CommandType = CommandType.StoredProcedure;

                //Agrego los parámetros con sus valores
                cmd.Parameters.AddWithValue("@ciCli", c.Cedula);
                cmd.Parameters.AddWithValue("@TCCli", c.Tarjeta);
                cmd.Parameters.AddWithValue("@nombreCli", c.Nombre);
                cmd.Parameters.AddWithValue("@dirCli", c.Direccion);
                cmd.Parameters.AddWithValue("@FNCli", c.FechaNac);
                cmd.Parameters.AddWithValue("@telCli", c.Telefono);

                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);
                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == -1)
                    throw new Exception("La cedula " + c.Cedula + " Ya se encuentra en el sistema.");
                if (valorRetorno == -2)
                    throw new Exception("La tarjeta " + c.Tarjeta + " Ya se encuentra en el sistema.");
                if (valorRetorno == -6)
                    throw new Exception("Error de SQL");
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

        public static void Eliminar(int pCedula)
        {
            SqlConnection cnn = new SqlConnection(Constantes.CONEXION);
            try
            {
                SqlCommand cmd = new SqlCommand("EliminarC", cnn);
                cmd.CommandType = CommandType.StoredProcedure;

                //Agrego los parámetros con sus valores
                cmd.Parameters.AddWithValue("@ciCli", pCedula);

                SqlParameter prmRetorno = new SqlParameter();
                prmRetorno.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(prmRetorno);
                cnn.Open();
                cmd.ExecuteNonQuery();
                int valorRetorno = (int)prmRetorno.Value;
                if (valorRetorno == -1)
                    throw new Exception("No se existe cliente con cedula " + pCedula);
                 if (valorRetorno == -6)
                    throw new Exception("Error de SQL");

            }
            catch (Exception ex)
            { throw ex; }
            finally
            { cnn.Close(); }
        }

    }
}
